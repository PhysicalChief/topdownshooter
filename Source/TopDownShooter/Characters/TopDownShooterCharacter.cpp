// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "Components/ArrowComponent.h"
#include "../Game/TopDownShooterGameInstance.h"
#include "../Game/TopDownShooterPlayerController.h"
#include "../Enviroment/Weapons/WeaponDefault.h"
#include "CharacterHealthComponent.h"
#include "../Game/TopDownShooterGameMode.h"

ATopDownShooterCharacter::ATopDownShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
	if (InventoryComponent) {
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATopDownShooterCharacter::InitWeapon);
	}

	CharacterHealthComponent = CreateDefaultSubobject<UCharacterHealthComponent>(TEXT("CharacterHealthComponent"));
	if (CharacterHealthComponent) {
		CharacterHealthComponent->OnDead.AddDynamic(this, &ATopDownShooterCharacter::CharacterDead);
	}

	StaminaComponent = CreateDefaultSubobject<UStaminaComponent>(TEXT("StaminaComponent"));

	// Create a decal in the world to show the cursor's location
	/*CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/BluePrint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());*/

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATopDownShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	//if (CursorToWorld != nullptr)
	//{
	//	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	//	{
	//		if (UWorld* World = GetWorld())
	//		{
	//			FHitResult HitResult;
	//			FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
	//			FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
	//			FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
	//			Params.AddIgnoredActor(this);
	//			World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
	//			FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
	//			CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
	//		}
	//	}
	//	else if (APlayerController* PC = Cast<APlayerController>(GetController()))
	//	{
	//		FHitResult TraceHitResult;
	//		PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
	//		FVector CursorFV = TraceHitResult.ImpactNormal;
	//		FRotator CursorR = CursorFV.Rotation();
	//		CursorToWorld->SetWorldLocation(TraceHitResult.Location);
	//		CursorToWorld->SetWorldRotation(CursorR);
	//	}
	//}

	if (CurrentCursor) {
		ATopDownShooterPlayerController* controller = Cast<ATopDownShooterPlayerController>(GetController());
		if (controller) {
			FHitResult hitResult;
			controller->GetHitResultUnderCursor(ECC_Visibility, true, hitResult);
			FVector cursorNormal = hitResult.ImpactNormal;
			FRotator cursorRotator = cursorNormal.Rotation();

			CurrentCursor->SetWorldLocation(hitResult.Location);
			CurrentCursor->SetWorldRotation(cursorRotator);
		}
	}

	MovementTick(DeltaSeconds);
	StaminaChange(DeltaSeconds);
	AnimStateTick(DeltaSeconds);
}

void ATopDownShooterCharacter::SetupPlayerInputComponent(UInputComponent* inputComponent)
{
	Super::SetupPlayerInputComponent(inputComponent);

	inputComponent->BindAxis(TEXT("MoveForward"), this, &ATopDownShooterCharacter::InputAxisX);
	inputComponent->BindAxis(TEXT("MoveRight"), this, &ATopDownShooterCharacter::InputAxisY);
	inputComponent->BindAxis(TEXT("MoveCameraHight"), this, &ATopDownShooterCharacter::InputMouseWheelUp);
	inputComponent->BindAction(TEXT("SprintButton"), IE_Pressed, this, &ATopDownShooterCharacter::SetIsSprint);
	inputComponent->BindAction(TEXT("SprintButton"), IE_Released, this, &ATopDownShooterCharacter::SetNotSprint);
	inputComponent->BindAction(TEXT("WalkButton"), IE_Pressed, this, &ATopDownShooterCharacter::SetIsWalk);
	inputComponent->BindAction(TEXT("WalkButton"), IE_Released, this, &ATopDownShooterCharacter::SetNotWalk);
	inputComponent->BindAction(TEXT("AimButton"), IE_Pressed, this, &ATopDownShooterCharacter::SetIsAim);
	inputComponent->BindAction(TEXT("AimButton"), IE_Released, this, &ATopDownShooterCharacter::SetNotAim);
	inputComponent->BindAction(TEXT("FireButton"), IE_Pressed, this, &ATopDownShooterCharacter::InputAttackPressed);
	inputComponent->BindAction(TEXT("FireButton"), IE_Released, this, &ATopDownShooterCharacter::InputAttackReleased);
	inputComponent->BindAction(TEXT("ReloadButton"), IE_Pressed, this, &ATopDownShooterCharacter::TryReloadWeapon);
	inputComponent->BindAction(TEXT("SwitchNextWeaponButton"), IE_Pressed, this, &ATopDownShooterCharacter::TrySwitchNextWeapon);
	inputComponent->BindAction(TEXT("SwitchPreviosWeaponButton"), IE_Pressed, this, &ATopDownShooterCharacter::TrySwitchPreviousWeapon);
	inputComponent->BindAction(TEXT("SwitchAmmoType"), IE_Pressed, this, &ATopDownShooterCharacter::TrySwitchCurrentAmmo);
}

void ATopDownShooterCharacter::CharacterDead()
{
	if (!bIsAlive) return;

	int8 rand = FMath::RandRange(0, DeathMontages.Num() - 1);
	float anim_time = 0.0f;
	if (DeathMontages[rand] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		anim_time = DeathMontages[rand]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeathMontages[rand]);
	}

	bIsAlive = false;

	UnPossessed();

	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATopDownShooterCharacter::EnableRagDoll, anim_time, false);
	GetCursorToWorld()->SetVisibility(false);
}

void ATopDownShooterCharacter::EnableRagDoll()
{
	if (auto mesh = GetMesh())
	{
		mesh->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		mesh->SetSimulatePhysics(true);
	}
}

void ATopDownShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (cursorMaterial) {
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), cursorMaterial, cursorSize, FVector(0));
	}
}

void ATopDownShooterCharacter::InputAxisX(float value)
{
	AxisX = value;
}

void ATopDownShooterCharacter::InputAxisY(float value)
{
	AxisY = value;
}

void ATopDownShooterCharacter::InputMouseWheelUp(float value)
{
	MouseWheelUp = value;
	if (MouseWheelUp == 0) return;
	if (bCameraInMovement) return;
	if (!bCameraSmoothMove) {
		MoveCamera();
	}
	else {
		lastCameraMoveDirection = MouseWheelUp;
		bCameraInMovement = true;
		cameraMoveDelegate.BindUObject(this, &ATopDownShooterCharacter::MoveCameraSmooth);
		GetWorld()->GetTimerManager().SetTimer(cameraMoveTimerHandler, cameraMoveDelegate, cameraMoveTick, true);
	}
}

void ATopDownShooterCharacter::MovementTick(float deltaTime)
{
	if (!bIsAlive) return;

	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	ATopDownShooterPlayerController* myController = Cast<ATopDownShooterPlayerController>(GetController());
	if (myController) {

		if (!bIsSprintState) {
			FHitResult hitResult;
			myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, hitResult);

			float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), hitResult.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));
			FVector Displacement = FVector(0, 0, CurrentWeapon->ShootLocation->GetComponentLocation().Z);

			CurrentWeapon->ShootEndLocation = hitResult.Location + Displacement;
		}
		else {
			FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
			FRotator myRotator = myRotationVector.ToOrientationRotator();
			SetActorRotation((FQuat(myRotator)));
		}
	}
}

void ATopDownShooterCharacter::AnimStateTick(float deltaTime)
{
	if (!bIsSprintState && bIsAimState) {
		CharacterUpperState = CurrentWeapon->WeaponSetting.AimStateBlendSpace;
	}
	else {
		CharacterUpperState = CurrentWeapon->WeaponSetting.StandartStateBlendSpace;
	}
}

void ATopDownShooterCharacter::CharacterUpdate()
{
	float resSpeed = 600.0f;
	switch (movementState)
	{
	case EMovementState::AimState:
		resSpeed = speedInfo.AimSpeed;
		CurrentWeapon->bShouldReduceDispersion = true;
		break;
	case EMovementState::WalkState:
		resSpeed = speedInfo.WalkSpeed;
		CurrentWeapon->bShouldReduceDispersion = true;
		break;
	case EMovementState::RunState:
		resSpeed = speedInfo.RunSpeed;
		CurrentWeapon->bShouldReduceDispersion = false;
		break;
	case EMovementState::WalkAimState:
		resSpeed = speedInfo.WalkAimState;
		CurrentWeapon->bShouldReduceDispersion = true;
		break;
	case EMovementState::SprintState:
		resSpeed = speedInfo.SprintState;
		CurrentWeapon->bShouldReduceDispersion = false;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = resSpeed;
}

void ATopDownShooterCharacter::ChangeMovementState()
{
	if (bIsSprintState) {
		movementState = EMovementState::SprintState;
		if (StaminaComponent)
		{
			StaminaComponent->UseSprintStamina(true);
		}
	}
	else if (!bIsSprintState && !bIsAimState && bIsWalkState)
	{
		movementState = EMovementState::WalkState;
		if (StaminaComponent)
		{
			StaminaComponent->UseSprintStamina(false);
		}
	}
	else if (!bIsSprintState && bIsAimState && bIsWalkState) {
		movementState = EMovementState::WalkAimState;
		if (StaminaComponent)
		{
			StaminaComponent->UseSprintStamina(false);
		}
	}
	else if (!bIsSprintState && bIsAimState && !bIsWalkState) {
		movementState = EMovementState::AimState;
		if (StaminaComponent)
		{
			StaminaComponent->UseSprintStamina(false);
		}
	}
	else if (!bIsAimState && !bIsWalkState && !bIsSprintState) {
		movementState = EMovementState::RunState;
		if (StaminaComponent)
		{
			StaminaComponent->UseSprintStamina(false);
		}
	}

	CharacterUpdate();

	AWeaponDefault* MyWeapon = GetCurrentWeapon();
	if (MyWeapon) {
		MyWeapon->UpdateStateWeapon(movementState);
	}

}

void ATopDownShooterCharacter::MoveCamera()
{
	float cameraOffset = MouseWheelUp * cameraMoveSteep;
	float currentCameraHight = CameraBoom->TargetArmLength;
	if (cameraOffset + currentCameraHight >= minCameraHight && cameraOffset + currentCameraHight <= maxCameraHight) {
		CameraBoom->TargetArmLength += cameraOffset;
	}
}

void ATopDownShooterCharacter::MoveCameraSmooth()
{
	float cameraOffset = lastCameraMoveDirection * cameraMoveSteepSlide;
	float currentCameraHight = CameraBoom->TargetArmLength;
	if (cameraOffset + currentCameraHight >= minCameraHight && cameraOffset + currentCameraHight <= maxCameraHight) {
		CameraBoom->TargetArmLength += cameraOffset;
	}
	currentCameraMoveDistance += abs(cameraOffset);
	if (currentCameraMoveDistance >= cameraMoveSteep) {
		GetWorld()->GetTimerManager().ClearTimer(cameraMoveTimerHandler);
		currentCameraMoveDistance = 0;
		lastCameraMoveDirection = 0.0f;
		bCameraInMovement = false;
	}
}

void ATopDownShooterCharacter::SetIsAim()
{
	bIsAimState = true;
	CurrentWeapon->bAimEnable = true;
	ChangeMovementState();
}

void ATopDownShooterCharacter::SetNotAim()
{
	bIsAimState = false;
	CurrentWeapon->bAimEnable = false;
	ChangeMovementState();
}

void ATopDownShooterCharacter::SetIsWalk()
{
	bIsWalkState = true;
	ChangeMovementState();
}

void ATopDownShooterCharacter::SetNotWalk()
{
	bIsWalkState = false;
	ChangeMovementState();
}

void ATopDownShooterCharacter::SetIsSprint()
{
	if (bIsCanSprint) {
		if (!StaminaComponent || (StaminaComponent && StaminaComponent->CanSprint()))
		{
			bIsSprintState = true;
			ChangeMovementState();
		}
	}
}

void ATopDownShooterCharacter::SetNotSprint()
{
	bIsSprintState = false;
	ChangeMovementState();
}

void ATopDownShooterCharacter::StaminaChange(float DeltaSecond)
{
	if (StaminaComponent)
	{
		if (StaminaComponent->CanSprint())
		{
			bIsCanSprint = true;
			bIsWalkState = false;
		}
		else
		{
			bIsCanSprint = false;
			bIsSprintState = false;
			bIsWalkState = true;
			ChangeMovementState();
		}
	}
	else {
		if (bIsSprintState) {
			stamina -= sprintChargePerSec * DeltaSecond;
		}
		else if (stamina < staminaMax) {
			stamina += staminaRecoveryPerSec * DeltaSecond;
		}
		if (stamina <= 0) {
			bIsCanSprint = false;
			bIsSprintState = false;
			bIsWalkState = true;
			ChangeMovementState();
		}
		else if (!bIsCanSprint && stamina >= 10) {
			bIsCanSprint = true;
			bIsWalkState = false;
		}
	}
}

void ATopDownShooterCharacter::InputAttackPressed()
{
	AttackCharacterEvent(true);
}

void ATopDownShooterCharacter::InputAttackReleased()
{
	AttackCharacterEvent(false);
}

void ATopDownShooterCharacter::AttackCharacterEvent(bool bIsFiring)
{
	AWeaponDefault* MyWeapon = nullptr;
	MyWeapon = GetCurrentWeapon();
	if (MyWeapon)
	{
		//ToDo Check melee or range
		MyWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

AWeaponDefault* ATopDownShooterCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

UDecalComponent* ATopDownShooterCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ATopDownShooterCharacter::InitWeapon(FName IdWeapon, FAddicionalWeaponInfo WeaponAdditionalInfo, int32 NewWeaponIndex)
{
	if (CurrentWeapon) {
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UTopDownShooterGameInstance* CurrentGameInstance = Cast<UTopDownShooterGameInstance>(GetGameInstance());
	FWeaponInfo NewWeaponInfo;
	if (CurrentGameInstance) {
		CurrentGameInstance->GetWeaponInfoByName(IdWeapon, NewWeaponInfo);
		if (NewWeaponInfo.WeaponClass) {
			FVector SpawnLocation = FVector(0);
			FRotator SpawnRotation = FRotator(0);

			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = GetInstigator();

			AWeaponDefault* MyWeapon = Cast<AWeaponDefault>(
				GetWorld()->SpawnActor(NewWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams)
			);
			if (MyWeapon) {
				MyWeapon->CurrentWeaponName = IdWeapon;
				FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
				MyWeapon->AttachToComponent(GetMesh(), Rule, FName("CharacterWaponSocket_r"));
				CurrentWeapon = MyWeapon;

				MyWeapon->UpdateStateWeapon(movementState);

				MyWeapon->WeaponSetting = NewWeaponInfo;
				MyWeapon->WeaponInfo.Round = NewWeaponInfo.MaxRound;
				MyWeapon->WeaponInfo = WeaponAdditionalInfo;
				if (InventoryComponent) {
					CurrentWeaponIndex = InventoryComponent->GetWeaponIndexSlotByName(IdWeapon);
				}
				//���� ����� ���������� ������������ ����� �������� ���������� ��� ������������ �� ������
				/*if (MyWeapon->WeaponInfo.Round > MyWeapon->WeaponSetting.MaxRound) {
					MyWeapon->WeaponInfo.Round = MyWeapon->WeaponSetting.MaxRound;
				}*/

				MyWeapon->OnWeaponReloadStartDelegate.AddDynamic(this, &ATopDownShooterCharacter::OnWeaponReloadStart);
				MyWeapon->OnWeaponReloadEndDelegate.AddDynamic(this, &ATopDownShooterCharacter::OnWeaponReloadEnd);
				MyWeapon->OnWeaponFireStartDelegate.AddDynamic(this, &ATopDownShooterCharacter::OnWeaponFireStart);

				CurrentWeaponIndex = NewWeaponIndex;

			}
		}
	}
}

void ATopDownShooterCharacter::TryReloadWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->bWeaponIsReloading)  {
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound) {
			if (InventoryComponent) {
				int32 Ammo = CurrentWeapon->WeaponSetting.MaxRound;
				if (InventoryComponent->NewAmmoSlots.Contains(CurrentWeapon->WeaponSetting.WeaponType) &&
					InventoryComponent->AmmoSlotIndex.Contains(CurrentWeapon->WeaponSetting.WeaponType)) {
					Ammo = InventoryComponent->NewAmmoSlots[CurrentWeapon->WeaponSetting.WeaponType].AmmoSlot[
						InventoryComponent->AmmoSlotIndex[CurrentWeapon->WeaponSetting.WeaponType]
					].AmmoAmount;
				}
				if (CurrentWeapon->CanWeaponReload()) 
					CurrentWeapon->InitWeaponReload();
			}
			else {
				CurrentWeapon->InitWeaponReload();
			}
		}
	}
}

void ATopDownShooterCharacter::OnWeaponReloadStart(UAnimMontage* Anim)
{
	OnWeaponReloadStart_BP(Anim);
}

void ATopDownShooterCharacter::OnWeaponReloadEnd(bool bIsSucces, int32 AmmoTake)
{
	if (InventoryComponent) {
		InventoryComponent->WeaponReloaded(CurrentWeapon->WeaponSetting.WeaponType, -AmmoTake);
		InventoryComponent->SetAdditionalWeaponInfo(CurrentWeaponIndex, CurrentWeapon->WeaponInfo);
	}
	OnWeaponReloadEnd_BP(bIsSucces);
}

void ATopDownShooterCharacter::OnWeaponFireStart(UAnimMontage* Anim)
{
	OnWeaponFireStart_BP(Anim);
	if (InventoryComponent) {
		InventoryComponent->SetAdditionalWeaponInfo(CurrentWeaponIndex, CurrentWeapon->WeaponInfo);
	}
}

void ATopDownShooterCharacter::TrySwitchNextWeapon()
{
	if (InventoryComponent && InventoryComponent->WeaponSlots.Num() > 1) {
		int8 OldIndex = CurrentWeaponIndex;
		FAddicionalWeaponInfo oldInfo;
		if (CurrentWeapon) {
			oldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->bWeaponIsReloading) {
				CurrentWeapon->CancelReload();
			}
		}
		if (InventoryComponent->SwitchWeaponToIndex(CurrentWeaponIndex + 1, OldIndex, oldInfo)) {

		}
	}
}

void ATopDownShooterCharacter::TrySwitchPreviousWeapon()
{
	if (InventoryComponent && InventoryComponent->WeaponSlots.Num() > 1) {
		int8 OldIndex = CurrentWeaponIndex;
		FAddicionalWeaponInfo oldInfo;
		if (CurrentWeapon) {
			oldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->bWeaponIsReloading) {
				CurrentWeapon->CancelReload();
			}
		}
		if (InventoryComponent->SwitchWeaponToIndex(CurrentWeaponIndex - 1, OldIndex, oldInfo)) {

		}
	}
}

void ATopDownShooterCharacter::TrySwitchCurrentAmmo()
{
	if (InventoryComponent && CurrentWeapon) {
		int32 oldWeaponRound = CurrentWeapon->GetWeaponRound();
		bool bTryResult = InventoryComponent->TrySwitchCurrentAmmoType(CurrentWeapon->WeaponSetting.WeaponType, oldWeaponRound);
		if (bTryResult) {
			CurrentWeapon->ChangeWeaponRound(-oldWeaponRound);
			if (CurrentWeapon->CanWeaponReload())
				CurrentWeapon->InitWeaponReload();
		}
	}
}

float ATopDownShooterCharacter::TakeDamage(
	float DamageAmount, 
	FDamageEvent const& DamageEvent, 
	AController* EventInstigator, 
	AActor* DamageCauser)
{
	if (!bIsAlive) return 0.0f;

	auto result = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	CharacterHealthComponent->ChangeHealthValue(-DamageAmount);
	return result;
}

void ATopDownShooterCharacter::OnWeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
}

void ATopDownShooterCharacter::OnWeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
}

void ATopDownShooterCharacter::OnWeaponReloadEnd_BP_Implementation(bool bIsSucces)
{
}
