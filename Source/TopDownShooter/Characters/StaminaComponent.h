// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "StaminaComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStaminaChange, float, Value, float, MaxValue);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOPDOWNSHOOTER_API UStaminaComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UStaminaComponent();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Stamina")
	FOnStaminaChange OnStaminaChange;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
	float Stamina = 100.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
	float MaxStamina = 100.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
	float StaminaRegenRate = 10.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
	float StaminaRegenDelay = 5.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
	float StaminaSprintCost = 10.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
	float StaminaJumpCost = 10.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
	float StaminaMinimumSprintCost = 10.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
	float StaminaMinimumJumpCost = 10.0f;

	FTimerHandle RestoreTimerHandle;
	bool bIsCanRestore = true;
	bool bIsSprinting = false;



public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Stamina")
	bool CanSprint();

	UFUNCTION(BlueprintCallable, Category = "Stamina")
	bool CanJump();

	UFUNCTION(BlueprintCallable, Category = "Stamina")
	void UseSprintStamina(bool bIsSprinting);

	UFUNCTION(BlueprintCallable, Category = "Stamina")
	void UseJumpStamina();

	UFUNCTION()
	void CanRestore();

	UFUNCTION(BlueprintCallable, Category = "Stamina")
	float GetStamina() const { return Stamina; }

	UFUNCTION(BlueprintCallable, Category = "Stamina")
	float GetMaxStamina() const { return MaxStamina; }
};
