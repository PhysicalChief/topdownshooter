// Fill out your copyright notice in the Description page of Project Settings.

#include "CharacterHealthComponent.h"
#include "Kismet/GameplayStatics.h"

UCharacterHealthComponent::UCharacterHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;
}

void UCharacterHealthComponent::_UpdateActor(float dt)
{
	if (bShieldRepairing)
	{
		auto change_value = dt * ShieldRepair;
		ShieldHealth += change_value;
		OnShieldChange.Broadcast(ShieldHealth, change_value);
		if (ShieldHealth >= ShieldHealthMax)
		{
			ShieldHealth = ShieldHealthMax;
		}
	}
}

void UCharacterHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetTimerManager().SetTimer(ShieldRepairTimer, this, &UCharacterHealthComponent::CanRepairShield, ShieldRepairDelay, false);
}

void UCharacterHealthComponent::ReceiveDamage(float DamageAmount)
{
	Super::ReceiveDamage(DamageAmount);
}

void UCharacterHealthComponent::ChangeHealthValue(float ChangeAmount)
{
	

	float currentDamage = ChangeAmount * CoefDamage;
	float resultDamage = ChangeAmount;

	if (ChangeAmount < 0)
	{
		if (ShieldHealth > 0.0f)
		{
			ShieldHealth += currentDamage;
			OnShieldChange.Broadcast(ShieldHealth, currentDamage);
			if (ShieldHealth <= 0)
			{
				//FX place
				resultDamage = ShieldHealth / CoefDamage;
				if (ShieldDestroyEffect)
				{
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ShieldDestroyEffect, GetOwner()->GetActorLocation());
				}
				ShieldHealth = 0;
			}
			else {
				resultDamage = 0;
			}
		}
		if (resultDamage < 0)
			Super::ChangeHealthValue(resultDamage);
	}
	else
		Super::ChangeHealthValue(resultDamage);

	bShieldRepairing = false;
	GetWorld()->GetTimerManager().SetTimer(ShieldRepairTimer, this, &UCharacterHealthComponent::CanRepairShield, ShieldRepairDelay, false);
}

float UCharacterHealthComponent::GetShieldHealth() const
{
	return ShieldHealth;
}

void UCharacterHealthComponent::SetShieldHealth(float NewShieldHealth)
{
	ShieldHealth = NewShieldHealth;
}

void UCharacterHealthComponent::ChangeeShieldHealth(float ChangeAmount)
{
	ShieldHealth += ChangeAmount;
}

void UCharacterHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	_UpdateActor(DeltaTime);
}
