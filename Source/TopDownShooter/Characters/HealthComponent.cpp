// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHealthComponent::ReceiveDamage(float DamageAmount)
{
	_Health -= DamageAmount;
	if (_Health <= 0.0f)
	{
		OnDead.Broadcast();
	}
	OnHealthChange.Broadcast(_Health, DamageAmount);
}

void UHealthComponent::SetHealth(float NewHealth)
{
	_Health = NewHealth;
	if (_Health <= 0.0f)
	{
		OnDead.Broadcast();
	}
}

void UHealthComponent::ChangeHealthValue(float ChangeAmount)
{
	ChangeAmount *= CoefDamage;
	_Health += ChangeAmount;
	if (_Health <= 0.0f)
	{
		OnDead.Broadcast();
	}
	else if (_Health > 100.0f)
	{
		_Health = 100.f;
	}
	OnHealthChange.Broadcast(_Health, ChangeAmount);
}

float UHealthComponent::GetHealth() const
{
	return _Health;
}

void UHealthComponent::DeadEvent_Implementation()
{
}

