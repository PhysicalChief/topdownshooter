// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <map>

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../FunctionLibrary/Types.h"
#include "InventoryComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnWeaponSwitch, FName, WeaponIdName, FAddicionalWeaponInfo, WeaponAdditionalInfo, int32, NewWeaponIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnAmmoChange, EWeaponType, WeaponTpye, int32, AmmoCount, FName, AmmoName);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChange, int, IndexSlot, FAddicionalWeaponInfo, AdditionalInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoEmpty, EWeaponType, WeaponTpye);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponSlotsUpdate, FWeaponSlot, WeaponSlot, int32, SlotIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnChangeCurrentAmmo, EWeaponType, WeaponType, int32, NewSlot, int32, OldSlot);


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOPDOWNSHOOTER_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()
public:
	// Sets default values for this component's properties
	UInventoryComponent();

	UPROPERTY(BlueprintAssignable)
	FOnWeaponSwitch OnSwitchWeapon;

	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnAmmoChange OnAmmoChange;

	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnWeaponAdditionalInfoChange OnAdditionalInfoChange;

	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnWeaponAmmoEmpty OnWeaponAmmoEmpty;

	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnChangeCurrentAmmo OnChangeCurrentAmmo;

	UPROPERTY(BlueprintAssignable, Category = "UI")
	FOnWeaponSlotsUpdate OnWeaponSlotsUpdate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	TArray<FWeaponSlot> WeaponSlots;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	//TArray<FAmmoSlot> AmmoSlots;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	TMap<EWeaponType, FAmmoSlotWeaponType> NewAmmoSlots;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	TMap<EWeaponType, int32> AmmoSlotIndex;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	int32 MaxSlotsWeapon = 0;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	bool SwitchWeaponToIndex(int32 Index, int32 OldIndex, FAddicionalWeaponInfo OldWeaponInfo);

	FAddicionalWeaponInfo GetAdditionalWeaponInfo(int32 Index);

	int32 GetWeaponIndexSlotByName(FName WeaponName);

	FName GetWeaponNameByIndex(int32 Index);

	void SetAdditionalWeaponInfo(int32 Index, FAddicionalWeaponInfo Info);

	void WeaponReloaded(EWeaponType TypeWeapon, int32 AmmoCount);

	bool CheckAmmoForWeapon(EWeaponType TypeWeapon, int32& AvailbleAmmo);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void AmmoSlotChangValue(EWeaponType TypeWeapon, int32 AmmoCount, FName dropName);

	//Interfacee Pick Up Actors
	UFUNCTION(BlueprintCallable, Category = "Pick Up")
	bool CanTakeWeapon(int32 &FreeSlot);

	UFUNCTION(BlueprintCallable, Category = "Pick Up")
	bool CanTakeAmmo(EWeaponType AmmoType, FName dropName);

	UFUNCTION(BlueprintCallable, Category = "Pick Up")
	bool SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurentIndexWeaponCharater, FDropItemStruct& DropItemStruct);

	UFUNCTION(BlueprintCallable, Category = "Pick Up")
	bool TryGetWeaponToInventory(FWeaponSlot NewWeapon);

	UFUNCTION(BlueprintCallable, Category = "Pick Up")
	bool GetDropItemInfoFromInventory(int32 IndexSlot, FDropItemStruct& DropItemInfo);

	UFUNCTION(BlueprintCallable, Category = "Ammo")
	bool TrySwitchCurrentAmmoType(EWeaponType TypeWeapon, int32 rollBackAmmo);

	UFUNCTION(BlueprintCallable, Category = "Ammo")
	void InitAmmoSlotsUi();
};
