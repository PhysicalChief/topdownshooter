// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HealthComponent.h"
#include "CharacterHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield , float, Damage);

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UCharacterHealthComponent : public UHealthComponent
{
	GENERATED_BODY()
	
public:
	UCharacterHealthComponent();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "CharacterHealth")
	FOnShieldChange OnShieldChange;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "CharacterHealth")
	float ShieldHealth = 0.0f;
	UPROPERTY(EditDefaultsOnly, Category = "CharacterHealth")
	float ShieldHealthMax = 100.0f;
	UPROPERTY(EditDefaultsOnly, Category = "CharacterHealth")
	float ShieldRepair = 10.0f;
	UPROPERTY(EditDefaultsOnly, Category = "CharacterHealth")
	float ShieldRepairDelay = 5.0f;
	FTimerHandle ShieldRepairTimer;
	bool bShieldRepairing = false;

	UPROPERTY(EditDefaultsOnly, Category = "CharacterHealth")
	UParticleSystem* ShieldDestroyEffect = nullptr;

private:

	void _UpdateActor(float dt);

protected:
	virtual void BeginPlay() override;

public:

	void ReceiveDamage(float DamageAmount) override;

	void ChangeHealthValue(float ChangeAmount) override;

	UFUNCTION(BlueprintCallable, Category = "CharacterHealth")
	float GetShieldHealth() const;
	UFUNCTION(BlueprintCallable, Category = "CharacterHealth")
	void SetShieldHealth(float NewShieldHealth);
	UFUNCTION(BlueprintCallable, Category = "CharacterHealth")
	void ChangeeShieldHealth(float ChangeAmount);

	UFUNCTION(BlueprintCallable, Category = "CharacterHealth")
	float GetShieldHealthMax() const { return ShieldHealthMax; }

	UFUNCTION(BlueprintCallable, Category = "CharacterHealth")
	void SetShieldHealthMax(float NewShieldHealthMax) { ShieldHealthMax = NewShieldHealthMax; }

	UFUNCTION(BlueprintCallable, Category = "CharacterHealth")
	void ChangeShieldHealthMax(float ChangeAmount) { ShieldHealthMax += ChangeAmount; }

	UFUNCTION(BlueprintCallable, Category = "CharacterHealth")
	float GetShieldRepair() const { return ShieldRepair; }

	UFUNCTION(BlueprintCallable, Category = "CharacterHealth")
	void SetShieldRepair(float NewShieldRepair) { ShieldRepair = NewShieldRepair; }

	UFUNCTION(BlueprintCallable, Category = "CharacterHealth")
	void ChangeShieldRepair(float ChangeAmount) { ShieldRepair += ChangeAmount; }

	UFUNCTION(BlueprintCallable, Category = "CharacterHealth")
	float GetShieldRepairDelay() const { return ShieldRepairDelay; }

	UFUNCTION(BlueprintCallable, Category = "CharacterHealth")
	void SetShieldRepairDelay(float NewShieldRepairDelay) { ShieldRepairDelay = NewShieldRepairDelay; }

	UFUNCTION(BlueprintCallable, Category = "CharacterHealth")
	void CanRepairShield() { bShieldRepairing = true; };

	void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
