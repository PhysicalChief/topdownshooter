// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "InventoryComponent.h"
#include "StaminaComponent.h"
#include "../FunctionLibrary/Types.h"
#include "TopDownShooterCharacter.generated.h"

class UCharacterHealthComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCharacterFire);

UCLASS(Blueprintable)
class ATopDownShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATopDownShooterCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* inputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	UInventoryComponent* InventoryComponent = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	UMaterialInterface* cursorMaterial = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	FVector cursorSize = FVector(20.0f, 40.0f, 40.0f);


	UDecalComponent* CurrentCursor = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	AWeaponDefault* CurrentWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
	FName InitWeaponName;

	UPROPERTY(BlueprintAssignable)
	FOnCharacterFire OnCharacterFireDelegate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health")
	UCharacterHealthComponent* CharacterHealthComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health")
	bool bIsAlive = true;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Stamina")
	UStaminaComponent* StaminaComponent = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Death Animations")
	TArray<UAnimMontage*> DeathMontages;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Death Animations")
	FTimerHandle TimerHandle_RagDollTimer;


private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;

	UFUNCTION()
	void CharacterDead();

	UFUNCTION()
	void EnableRagDoll();

public:
	void BeginPlay() override;

	UFUNCTION()
		void InputAxisX(float value);

	UFUNCTION()
		void InputAxisY(float value);

	UFUNCTION()
		void InputMouseWheelUp(float value);

	UFUNCTION()
		void MovementTick(float deltaTime);

	UFUNCTION()
	void AnimStateTick(float deltaTime);

	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();

	UFUNCTION(BlueprintCallable)
		void MoveCamera();

	UFUNCTION(BlueprintCallable)
		void MoveCameraSmooth();

	UFUNCTION(BlueprintCallable)
		void SetIsAim();
	UFUNCTION(BlueprintCallable)
		void SetNotAim();
	UFUNCTION(BlueprintCallable)
		void SetIsWalk();
	UFUNCTION(BlueprintCallable)
		void SetNotWalk();
	UFUNCTION(BlueprintCallable)
		void SetIsSprint();
	UFUNCTION(BlueprintCallable)
		void SetNotSprint();

	UFUNCTION(BlueprintCallable)
		void StaminaChange(float DeltaSecond);

	UFUNCTION()
	void InputAttackPressed();
	UFUNCTION()
	void InputAttackReleased();
	UFUNCTION(BlueprintCallable)
	void AttackCharacterEvent(bool bIsFiring);


	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon();


	float AxisX = 0.0f;
	float AxisY = 0.0f;
	float MouseWheelUp = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	int CurrentWeaponIndex = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterAnimState")
	EPlayerUpperStateBlendSpace CharacterUpperState = EPlayerUpperStateBlendSpace::OneHandBlendSpace;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterState")
		bool bIsAimState = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterState")
		bool bIsSprintState = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterState")
		bool bIsWalkState = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterState")
		float stamina = 100;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterState")
		float staminaMax = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterState")
		float sprintChargePerSec = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterState")
		float staminaRecoveryPerSec = 2;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterState")
		bool bIsCanSprint = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterState")
	bool bIsReloading = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState movementState = EMovementState::RunState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed speedInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float minCameraHight = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float maxCameraHight = 1500.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float cameraMoveSteep = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float cameraMoveSteepSlide = 1.0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float cameraMoveTick = 0.01f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float currentCameraMoveDistance = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		bool bCameraSmoothMove = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		bool bCameraInMovement = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float lastCameraMoveDirection = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		FTimerHandle cameraMoveTimerHandler;

	FTimerDelegate cameraMoveDelegate;

	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void InitWeapon(FName IdWeapon, FAddicionalWeaponInfo WeaponAdditionalInfo, int32 NewWeaponIndex);

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void TryReloadWeapon();

	UFUNCTION()
	void OnWeaponReloadStart(UAnimMontage* Anim);

	UFUNCTION()
	void OnWeaponReloadEnd(bool bIsSucces, int32 AmmoTake );

	UFUNCTION()
	void OnWeaponFireStart(UAnimMontage* Anim);

	UFUNCTION(BlueprintNativeEvent, Category = "Weapon")
	void OnWeaponReloadStart_BP(UAnimMontage* Anim);

	UFUNCTION(BlueprintNativeEvent, Category = "Weapon")
	void OnWeaponReloadEnd_BP(bool bIsSucces);

	UFUNCTION(BlueprintNativeEvent, Category = "Weapon")
	void OnWeaponFireStart_BP(UAnimMontage* Anim);

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void TrySwitchNextWeapon();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void TrySwitchPreviousWeapon();

	UFUNCTION(BlueprintCallable, Category = "Ammo") 
	void TrySwitchCurrentAmmo();

	float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
};

