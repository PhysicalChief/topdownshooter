// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryComponent.h"
#include "../Game/TopDownShooterGameInstance.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	UTopDownShooterGameInstance* GI = GetWorld()->GetGameInstance<UTopDownShooterGameInstance>();

	for (int i = 0; i < WeaponSlots.Num(); i++) {
		if (GI) {
			if (!WeaponSlots[i].NameItem.IsNone()) {
				FWeaponInfo info;
				if (GI->GetWeaponInfoByName(WeaponSlots[i].NameItem, info)) {
					WeaponSlots[i].AdditionalInfo.Round = info.MaxRound;
				}
				else {
					/*WeaponSlots.RemoveAt(i);
					i--;*/
				}
			}
		}
	}

	MaxSlotsWeapon = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0)) {
		if (!WeaponSlots[0].NameItem.IsNone()) {
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
		}
	}
}


// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UInventoryComponent::SwitchWeaponToIndex(int32 Index, int32 OldIndex, FAddicionalWeaponInfo OldWeaponInfo)
{
	bool bResult = false;
	int8 CorrectIndex = Index;
	if (CorrectIndex < 0) {
		CorrectIndex = WeaponSlots.Num() - 1;
	}
	if (CorrectIndex >= WeaponSlots.Num()) {
		CorrectIndex = 0;
	}

	FName NewIdWeapon;
	FAddicionalWeaponInfo NewAdditionalInfo;

	if (WeaponSlots.IsValidIndex(CorrectIndex)) {
		NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
		NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
		bResult = true;
	}

	if (bResult) {
		SetAdditionalWeaponInfo(OldIndex, OldWeaponInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, CorrectIndex);
	}
	if (!bResult) {

	}
	return bResult;
}

FAddicionalWeaponInfo UInventoryComponent::GetAdditionalWeaponInfo(int32 Index)
{
	return FAddicionalWeaponInfo();
}

int32 UInventoryComponent::GetWeaponIndexSlotByName(FName WeaponName)
{
	for (int i = 0; i < WeaponSlots.Num(); i++) {
		if (WeaponSlots[i].NameItem == WeaponName) {
			return i;
		}
	}
	return -1;
}

FName UInventoryComponent::GetWeaponNameByIndex(int32 Index)
{
	if (WeaponSlots.IsValidIndex(Index)) {
		return WeaponSlots[Index].NameItem;
	}
	return FName();
}

void UInventoryComponent::SetAdditionalWeaponInfo(int32 Index, FAddicionalWeaponInfo Info)
{
	if (WeaponSlots.IsValidIndex(Index)) {
		WeaponSlots[Index].AdditionalInfo = Info;
		OnAdditionalInfoChange.Broadcast(Index, Info);

	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Index %d is not valid in inventory!"), Index);
	}
}

void UInventoryComponent::WeaponReloaded(EWeaponType TypeWeapon, int32 AmmoCount)
{
	if (NewAmmoSlots.Contains(TypeWeapon) && AmmoSlotIndex.Contains(TypeWeapon)) {
		NewAmmoSlots[TypeWeapon].AmmoSlot[AmmoSlotIndex[TypeWeapon]].AmmoAmount -= AmmoCount;
		OnAmmoChange.Broadcast(TypeWeapon, NewAmmoSlots[TypeWeapon].AmmoSlot[AmmoSlotIndex[TypeWeapon]].AmmoAmount, NewAmmoSlots[TypeWeapon].AmmoSlot[AmmoSlotIndex[TypeWeapon]].ProjectileName);
	}
}

bool UInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int32 &AvailableAmmo)
{
	if (NewAmmoSlots.Contains(TypeWeapon) && AmmoSlotIndex.Contains(TypeWeapon)) {
		AvailableAmmo = NewAmmoSlots[TypeWeapon].AmmoSlot[AmmoSlotIndex[TypeWeapon]].AmmoAmount;
		return true;
	}
	OnWeaponAmmoEmpty.Broadcast(TypeWeapon);
	return false;
}

void UInventoryComponent::AmmoSlotChangValue(EWeaponType TypeWeapon, int32 AmmoCount, FName projectileName)
{
	if (NewAmmoSlots.Contains(TypeWeapon) && AmmoSlotIndex.Contains(TypeWeapon)) {
		for (int i = 0; i < NewAmmoSlots[TypeWeapon].AmmoSlot.Num(); i++) {
			if (NewAmmoSlots[TypeWeapon].AmmoSlot[i].ProjectileName == projectileName) {
				NewAmmoSlots[TypeWeapon].AmmoSlot[i].AmmoAmount += AmmoCount;
				if (NewAmmoSlots[TypeWeapon].AmmoSlot[i].AmmoAmount > NewAmmoSlots[TypeWeapon].AmmoSlot[i].MaxAmmoAmount) {
					NewAmmoSlots[TypeWeapon].AmmoSlot[i].AmmoAmount = NewAmmoSlots[TypeWeapon].AmmoSlot[i].MaxAmmoAmount;
				}
				OnAmmoChange.Broadcast(TypeWeapon, NewAmmoSlots[TypeWeapon].AmmoSlot[i].AmmoAmount, NewAmmoSlots[TypeWeapon].AmmoSlot[i].ProjectileName);
				break;
			}
		}
	}
}

bool UInventoryComponent::CanTakeWeapon(int32& FreeSlot)
{
	for (int i = 0; i < WeaponSlots.Num(); i++) {
		if (WeaponSlots[i].NameItem.IsNone()) {
			FreeSlot = i;
			return true;
		}
	}
	return false;
}

bool UInventoryComponent::CanTakeAmmo(EWeaponType AmmoType, FName projectileName)
{
	bool result = 0;
	if (NewAmmoSlots.Contains(AmmoType) && AmmoSlotIndex.Contains(AmmoType)) {
		for (int i = 0; i < NewAmmoSlots[AmmoType].AmmoSlot.Num(); i++) {
			if (NewAmmoSlots[AmmoType].AmmoSlot[i].ProjectileName == projectileName) {
				result = NewAmmoSlots[AmmoType].AmmoSlot[i].AmmoAmount < NewAmmoSlots[AmmoType].AmmoSlot[i].MaxAmmoAmount;
				break;
			}
		}
	}
	return result;
}

bool UInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurentIndexWeaponCharater, FDropItemStruct& DropItemStruct)
{
	if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemStruct)) {
		WeaponSlots[IndexSlot] = NewWeapon;
		SwitchWeaponToIndex(CurentIndexWeaponCharater, -1, NewWeapon.AdditionalInfo);
		OnWeaponSlotsUpdate.Broadcast(NewWeapon, IndexSlot);
		return true;
	}
	return false;
}

bool UInventoryComponent::TryGetWeaponToInventory(FWeaponSlot NewWeapon)
{
	int32 FreeSlot = -1;
	if (CanTakeWeapon(FreeSlot)) {
		if (WeaponSlots.IsValidIndex(FreeSlot)) {
			WeaponSlots[FreeSlot] = NewWeapon;
			OnWeaponSlotsUpdate.Broadcast(NewWeapon, FreeSlot);
			return true;
		}
	}
	return false;
}

bool UInventoryComponent::GetDropItemInfoFromInventory(int32 IndexSlot, FDropItemStruct& DropItemInfo)
{
	FName DropName = GetWeaponNameByIndex(IndexSlot);
	if (!DropName.IsNone()) {
		UTopDownShooterGameInstance* GameInstance = GetWorld()->GetGameInstance<UTopDownShooterGameInstance>();
		if (GameInstance) {
			bool bResult = GameInstance->GetDropItemInfoByName(DropName, DropItemInfo);
			if (bResult) {
				DropItemInfo.WeaponSlot.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
			}
			return bResult;
		}
	}
	return false;
}

bool UInventoryComponent::TrySwitchCurrentAmmoType(EWeaponType TypeWeapon, int32 rollbackAmmo)
{
	if (NewAmmoSlots.Contains(TypeWeapon) && AmmoSlotIndex.Contains(TypeWeapon)) {
		int index = AmmoSlotIndex[TypeWeapon];
		if (NewAmmoSlots[TypeWeapon].AmmoSlot.Num() > 1) {
			AmmoSlotIndex[TypeWeapon] += 1;
			if (AmmoSlotIndex[TypeWeapon] >= NewAmmoSlots[TypeWeapon].AmmoSlot.Num()) {
				AmmoSlotIndex[TypeWeapon] = 0;
			}
			NewAmmoSlots[TypeWeapon].AmmoSlot[index].AmmoAmount += rollbackAmmo;
			OnAmmoChange.Broadcast(TypeWeapon, NewAmmoSlots[TypeWeapon].AmmoSlot[index].AmmoAmount, NewAmmoSlots[TypeWeapon].AmmoSlot[index].ProjectileName);
			OnChangeCurrentAmmo.Broadcast(TypeWeapon, AmmoSlotIndex[TypeWeapon], index);
			return true;
		}
	}
	return false;
}

void UInventoryComponent::InitAmmoSlotsUi()
{
	for (auto el : NewAmmoSlots) {
		OnChangeCurrentAmmo.Broadcast(el.Key, 0, 0);
	}
}

