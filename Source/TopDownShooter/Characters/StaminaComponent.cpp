// Fill out your copyright notice in the Description page of Project Settings.


#include "StaminaComponent.h"

// Sets default values for this component's properties
UStaminaComponent::UStaminaComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;

	// ...
}


// Called when the game starts
void UStaminaComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	GetWorld()->GetTimerManager().SetTimer(RestoreTimerHandle, this, &UStaminaComponent::CanRestore, StaminaRegenDelay, true);
}


// Called every frame
void UStaminaComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	if (bIsCanRestore && !bIsSprinting)
	{
		Stamina += StaminaRegenRate * DeltaTime;
		if (Stamina > MaxStamina)
		{
			Stamina = MaxStamina;
		}
		OnStaminaChange.Broadcast(Stamina, MaxStamina);
	}
	if (bIsSprinting)
	{
		bIsCanRestore = false;
		Stamina -= StaminaSprintCost * DeltaTime;
		if (Stamina < 0)
		{
			Stamina = 0;
		}
		OnStaminaChange.Broadcast(Stamina, MaxStamina);
	}
}

bool UStaminaComponent::CanSprint()
{
	return (Stamina > StaminaMinimumSprintCost) || (bIsSprinting && Stamina > 0);
}

bool UStaminaComponent::CanJump()
{
	return Stamina > StaminaMinimumJumpCost;
}

void UStaminaComponent::UseSprintStamina(bool _bIsSprinting)
{
	if (_bIsSprinting)
	{
		bIsCanRestore = false;
		bIsSprinting = true;
		GetWorld()->GetTimerManager().ClearTimer(RestoreTimerHandle);
	}
	else {
		bIsCanRestore = true;
		bIsSprinting = false;
		GetWorld()->GetTimerManager().SetTimer(RestoreTimerHandle, this, &UStaminaComponent::CanRestore, StaminaRegenDelay, true);
	}
}

void UStaminaComponent::UseJumpStamina()
{
	bIsCanRestore = false;
	GetWorld()->GetTimerManager().SetTimer(RestoreTimerHandle, this, &UStaminaComponent::CanRestore, StaminaRegenDelay, true);
	Stamina -= StaminaJumpCost;
	if (Stamina < 0)
	{
		Stamina = 0;
	}	
}

void UStaminaComponent::CanRestore()
{
	bIsCanRestore = true;
}

