// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

USTRUCT(BluePrintType)
struct FStatsParam
{
	GENERATED_BODY()

};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOPDOWNSHOOTER_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "HealthComponent")
	FOnHealthChange OnHealthChange;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "HealthComponent")
	FOnDead OnDead;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HealthComponent")
	float CoefDamage = 1.0f;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HealthComponent")
	float _Health = 100.f;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "HealthComponent")
	virtual void ReceiveDamage(float DamageAmount);

	UFUNCTION(BlueprintNativeEvent, Category = "HealthComponent")
	void DeadEvent();

	UFUNCTION(BlueprintCallable, Category = "HealthComponent")
	void SetHealth(float NewHealth);

	UFUNCTION(BlueprintCallable, Category = "HealthComponent")
	virtual void ChangeHealthValue(float ChangeAmount);

	UFUNCTION(BlueprintCallable, Category = "HealthComponent")
	float GetHealth() const;
};
