// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include "Projectile/ProjectileDefault.h"
#include "Components/ArrowComponent.h"
#include "../../Game/TopDownShooterGameInstance.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include <Engine/StaticMeshActor.h>
#include "../../Characters/InventoryComponent.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshComponent"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshWeapon"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	ShellDtopTick(DeltaTime);
	BulletDropTick(DeltaTime);
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh) {
		SkeletalMeshWeapon->DestroyComponent(true);
	}
	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh()) {
		StaticMeshWeapon->DestroyComponent(true);
	}
	if (SkeletalMeshWeapon) {
		AnimInstance = SkeletalMeshWeapon->GetAnimInstance();
	}
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (bWeaponFiring && GetWeaponRound() > 0 && !bWeaponIsReloading)
	{
		if (FireTimer < 0.f)
		{
			Fire();
		}
		else
			FireTimer -= DeltaTime;
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (bWeaponIsReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!bWeaponIsReloading) {
		if (!bWeaponFiring) {
			if (bShouldReduceDispersion) {
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			}
			else {
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
			}
		}
		if (CurrentDispersion < CurrentDispersionMin) {
			CurrentDispersion = CurrentDispersionMin;
		}
		else if (CurrentDispersion > CurrentDispersionMax) {
			CurrentDispersion = CurrentDispersionMax;
		}
	}
	if (ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire()) {
		bWeaponFiring = bIsFire;
	}
	else {
		bWeaponFiring = false;
		FireTimer = 0.01f;
	}
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !bBlockFire;
}

FProjectileInfo AWeaponDefault::GetProjectileInfo()
{
	return WeaponSetting.ProjectileSetting;
}

void AWeaponDefault::Fire()
{
	FireTimer = WeaponSetting.RateOfFire;

	ChangeWeaponRound(-1);
	ChangeDispersionByShot();

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentTransform());

	if (ShootLocation) {
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotator = ShootLocation->GetComponentRotation();

		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectileInfo();

		for (int i = 0; i < GetCountProjectileByShoot(); i++) {
			FVector endLocation = GetFireEndLocation();

			if (ProjectileInfo.ProjectileSubclass) {
				FVector ShootDirection = endLocation - SpawnLocation;
				ShootDirection.Normalize();

				FMatrix myMatrix = FMatrix(ShootDirection, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotator = myMatrix.Rotator();

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* NewProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.ProjectileSubclass,
					&SpawnLocation,
					&SpawnRotator,
					SpawnParams));
				if (NewProjectile) {
					NewProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
				}
			}
			else {
				FHitResult HitResult;
				TArray<AActor*> Actors;

				UKismetSystemLibrary::LineTraceSingle(
					GetWorld(),
					SpawnLocation,
					endLocation * WeaponSetting.DistacneTrace,
					ETraceTypeQuery::TraceTypeQuery4,
					false,
					Actors,
					EDrawDebugTrace::ForDuration,
					HitResult,
					true,
					FLinearColor::Red,
					FLinearColor::Green,
					5.0f
					);
				if (ShowDebug) {
					DrawDebugLine(
						GetWorld(),
						SpawnLocation,
						SpawnLocation + ShootLocation->GetForwardVector() * WeaponSetting.DistacneTrace,
						FColor::Black,
						false,
						(uint8)'\000',
						0.5f
						);
				}

				if (HitResult.GetActor() && HitResult.PhysMaterial.IsValid()) {
					EPhysicalSurface targetSurface = UGameplayStatics::GetSurfaceType(HitResult);
					if (WeaponSetting.ProjectileSetting.HitDecals.Contains(targetSurface)) {
						UMaterialInterface* DecalMaterial = WeaponSetting.ProjectileSetting.HitDecals[targetSurface];
						if (DecalMaterial && HitResult.GetComponent()) {
							UGameplayStatics::SpawnDecalAttached(
								DecalMaterial,
								FVector(20.f),
								HitResult.GetComponent(),
								NAME_None,
								HitResult.ImpactPoint,
								HitResult.ImpactNormal.Rotation(),
								EAttachLocation::KeepWorldPosition,
								15.f
							);
						}

					}
					if (WeaponSetting.ProjectileSetting.HitFXs.Contains(targetSurface)) {
						UParticleSystem* hitFX = WeaponSetting.ProjectileSetting.HitFXs[targetSurface];

						if (hitFX) {
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),
								hitFX,
								FTransform(HitResult.ImpactNormal.Rotation(),
									HitResult.ImpactPoint,
									FVector(1.0f)
								)
							);
						}
					}
					if (WeaponSetting.ProjectileSetting.ProjectileHitSound) {
						UGameplayStatics::PlaySoundAtLocation(
							GetWorld(),
							WeaponSetting.ProjectileSetting.ProjectileHitSound,
							HitResult.ImpactPoint
						);
					}
					UGameplayStatics::ApplyDamage(
						HitResult.GetActor(),
						WeaponSetting.ProjectileSetting.ProjectileDamage,
						GetInstigatorController(),
						this,
						NULL
					);
				}
			}
		}
		UAnimMontage* current = WeaponSetting.WeaponAnim.CharaterFireAnim;
		if (bAimEnable) {
			current = WeaponSetting.WeaponAnim.CharacterAimFireAnim;
		}
		OnWeaponFireStartDelegate.Broadcast(current);
		AnimInstance->Montage_Play(WeaponSetting.WeaponAnim.WeaponFireAnim);

		if (WeaponSetting.DropMeshBullet.DropMesh) {
			if (WeaponSetting.DropMeshBullet.DropMeshTime <= 0.0f) {
				InitDropMesh(
					WeaponSetting.DropMeshBullet.DropMesh,
					WeaponSetting.DropMeshBullet.DropMeshOffset,
					WeaponSetting.DropMeshBullet.DropMeshImpulseDirection,
					WeaponSetting.DropMeshBullet.DropMeshLifeTime,
					WeaponSetting.DropMeshBullet.ImpulseDispersion,
					WeaponSetting.DropMeshBullet.DropMeshImpulse,
					WeaponSetting.DropMeshBullet.CustomMass
				);
			}
			else {
				bBulletDropFlag = true;
				BulletDropTimer = WeaponSetting.DropMeshBullet.DropMeshTime;
			}
		}
	}

	if (GetWeaponRound() <= 0 && !bWeaponIsReloading)
	{
		//Init Reload
		if (CanWeaponReload())
			InitWeaponReload();
	}
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	bBlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::AimState:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
	case EMovementState::WalkState:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Walk_StateDispersionReduction;
		break;
	case EMovementState::WalkAimState:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionReduction;
		break;
	case EMovementState::RunState:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Run_StateDispersionReduction;
		break;
	case EMovementState::SprintState:
		bBlockFire = true;
		SetWeaponStateFire(false);
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersion()
{
}

void AWeaponDefault::SetWeaponSetting(FWeaponInfo NewWeaponinfo)
{
	WeaponSetting = NewWeaponinfo;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return WeaponInfo.Round;
}

void AWeaponDefault::ChangeWeaponRound(int32 Count)
{
	WeaponInfo.Round += Count;
}

void AWeaponDefault::InitWeaponReload()
{
	bWeaponIsReloading = true;

	ReloadTimer = WeaponSetting.ReloadTime;

	UAnimMontage* current = WeaponSetting.WeaponAnim.CharacterReloadAnim;
	UAnimMontage* weaponAnim = WeaponSetting.WeaponAnim.WeaponReloadAnim;
	if (bAimEnable) {
		current = WeaponSetting.WeaponAnim.CharacterAimReloadAnim;
		weaponAnim = WeaponSetting.WeaponAnim.WeaponAimReloadAnim;
	}
	OnWeaponReloadStartDelegate.Broadcast(current);
	AnimInstance->Montage_Play(weaponAnim);

	if (WeaponSetting.DropMeshMagazine.DropMesh) {
		if (WeaponSetting.DropMeshMagazine.DropMeshTime <= 0.0f) {
			InitDropMesh(
				WeaponSetting.DropMeshMagazine.DropMesh,
				WeaponSetting.DropMeshMagazine.DropMeshOffset,
				WeaponSetting.DropMeshMagazine.DropMeshImpulseDirection,
				WeaponSetting.DropMeshMagazine.DropMeshLifeTime,
				WeaponSetting.DropMeshMagazine.ImpulseDispersion,
				WeaponSetting.DropMeshMagazine.DropMeshImpulse,
				WeaponSetting.DropMeshMagazine.CustomMass
			);
		}
		else {
			bShellDropFlag = true;
			ShellDropTimer = WeaponSetting.DropMeshMagazine.DropMeshTime;
		}
	}
}

void AWeaponDefault::FinishReload()
{
	bWeaponIsReloading = false;
	int32 AvailableAmmo = GetAvailableAmmoForReload();
	if (AvailableAmmo > WeaponSetting.MaxRound) {
		AvailableAmmo = WeaponSetting.MaxRound;
	}

	int32 ReloadCount = WeaponInfo.Round - AvailableAmmo;
	WeaponInfo.Round = AvailableAmmo;
	

	OnWeaponReloadEndDelegate.Broadcast(true, ReloadCount);

	AnimInstance->Montage_Play(nullptr);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;

	FVector EndLocation = FVector::ZeroVector;

	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);

	if (tmpV.Size() > SizeVectorToChangeShootDirectionalLogic) {
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSetting.DistacneTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
	else {
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistacneTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}

	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

		//DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}

	return EndLocation;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionoShoot) const
{
	return FMath::VRandCone(DirectionoShoot, GetCurrentDispersion() * PI / 180.0f);
}

float AWeaponDefault::GetCurrentDispersion() const
{
	return CurrentDispersion;
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion += CurrentDispersionRecoil;
}

int8 AWeaponDefault::GetCountProjectileByShoot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

void AWeaponDefault::ShellDtopTick(float DeltaTime)
{
	if (bShellDropFlag) {
		if (ShellDropTimer < 0) {
			bShellDropFlag = false;
			InitDropMesh(
				WeaponSetting.DropMeshBullet.DropMesh,
				WeaponSetting.DropMeshBullet.DropMeshOffset,
				WeaponSetting.DropMeshBullet.DropMeshImpulseDirection,
				WeaponSetting.DropMeshBullet.DropMeshLifeTime,
				WeaponSetting.DropMeshBullet.ImpulseDispersion,
				WeaponSetting.DropMeshBullet.DropMeshImpulse,
				WeaponSetting.DropMeshBullet.CustomMass
			);
		}
		else {
			ShellDropTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::BulletDropTick(float DeltaTime)
{
	if (bBulletDropFlag) {
		if (BulletDropTimer < 0) {
			bBulletDropFlag = false;
			InitDropMesh(
				WeaponSetting.DropMeshBullet.DropMesh,
				WeaponSetting.DropMeshBullet.DropMeshOffset,
				WeaponSetting.DropMeshBullet.DropMeshImpulseDirection,
				WeaponSetting.DropMeshBullet.DropMeshLifeTime,
				WeaponSetting.DropMeshBullet.ImpulseDispersion,
				WeaponSetting.DropMeshBullet.DropMeshImpulse,
				WeaponSetting.DropMeshBullet.CustomMass
			);
		}
		else {
			BulletDropTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::InitDropMesh(UStaticMesh* Mesh, FTransform Offset, FVector DropImpulse, float LifeTime, float RandomInpulseDirection, float PowerImpulse, float CustomMuss)
{
	if (Mesh) {
		FTransform Transform;
		FVector OffsetLocation = Offset.GetLocation();
		FVector LocalDir = this->GetActorForwardVector() * OffsetLocation.X + this->GetActorRightVector() * OffsetLocation.Y + this->GetActorUpVector() * OffsetLocation.Z;
		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(Offset.GetScale3D());
		Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());
		AStaticMeshActor* NewActor = nullptr;
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		SpawnParams.Owner = this;
		NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, SpawnParams);
		if (NewActor && NewActor->GetStaticMeshComponent()) {
			UStaticMeshComponent* CreatedMesh = NewActor->GetStaticMeshComponent();
			CreatedMesh->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
			CreatedMesh->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
			//NewActor->SetActorTickEnabled(false);
			NewActor->SetLifeSpan(LifeTime);
			CreatedMesh->Mobility = EComponentMobility::Movable;
			CreatedMesh->SetSimulatePhysics(true);
			CreatedMesh->SetEnableGravity(true);
			CreatedMesh->SetStaticMesh(Mesh);

			CreatedMesh->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
			CreatedMesh->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
			CreatedMesh->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
			CreatedMesh->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
			CreatedMesh->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
			CreatedMesh->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

			if (CustomMuss > 0) {
				CreatedMesh->SetMassOverrideInKg(NAME_None, CustomMuss);
			}
			if (!DropImpulse.IsNearlyZero()) {
				FVector FinalDir(0.f);
				LocalDir = LocalDir + (DropImpulse * 1000.f);
				if (!FMath::IsNearlyZero(RandomInpulseDirection)) {
					FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, RandomInpulseDirection);
				}
				FinalDir.GetSafeNormal(0.0001f);
				CreatedMesh->AddImpulse(FinalDir * PowerImpulse, NAME_None, true);
			}
		}
	}
}

void AWeaponDefault::CancelReload()
{
	bWeaponIsReloading = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance()) {
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);
	}
	OnWeaponReloadEndDelegate.Broadcast(false, 0);
	bShellDropFlag = false;
}

bool AWeaponDefault::CanWeaponReload()
{
	bool bCanReload = true;
	if (GetOwner()) {
		UInventoryComponent* MyInventory = Cast<UInventoryComponent>(GetOwner()->GetComponentByClass(UInventoryComponent::StaticClass()));
		if (MyInventory) {
			int32 ammo;
			bCanReload = MyInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType, ammo);
		}
	}
	return bCanReload;
}

int32 AWeaponDefault::GetAvailableAmmoForReload()
{
	int32 ResultAmmo = WeaponSetting.MaxRound;
	if (GetOwner()) {
		UInventoryComponent* MyInventory = Cast<UInventoryComponent>(GetOwner()->GetComponentByClass(UInventoryComponent::StaticClass()));
		if (MyInventory) {
			bool bCanReload = MyInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType, ResultAmmo);
		}
	}
	return ResultAmmo;
}
