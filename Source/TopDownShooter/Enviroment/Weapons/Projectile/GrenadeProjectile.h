// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileDefault.h"
#include "GrenadeProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API AGrenadeProjectile : public AProjectileDefault
{
	GENERATED_BODY()

public:
	bool bTimerEnabled = false;

	float TimerToExplose = 0.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Explode")
	float TimeToExplose = 5.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Movement")
	int MaxBounceCount = 5;

private:
	int CurrentBounceCount = 0;

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float deltaTime) override;

	void TimerExplode(float deltaTime);

	virtual void ProjectileCollisionSphereHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	
	virtual void ImpactProjectile() override;

	void Explode();

};
