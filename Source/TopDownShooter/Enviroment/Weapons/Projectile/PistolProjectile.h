// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileDefault.h"
#include "PistolProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API APistolProjectile : public AProjectileDefault
{
	GENERATED_BODY()
	
};
