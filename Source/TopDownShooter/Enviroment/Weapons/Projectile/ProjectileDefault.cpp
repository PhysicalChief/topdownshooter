// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault.h"

#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "../../../Game/TopDownShooterGameInstance.h"

// Sets default values
AProjectileDefault::AProjectileDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	projectileSphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	projectileSphereCollision->SetSphereRadius(16.0f);
	projectileSphereCollision->bReturnMaterialOnMove = true;
	projectileSphereCollision->SetCanEverAffectNavigation(false);
	projectileSphereCollision->SetCollisionProfileName(TEXT("Projectile"), true);

	RootComponent = projectileSphereCollision;

	projectileMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile Mesh Component"));
	projectileMeshComponent->SetupAttachment(RootComponent);
	projectileMeshComponent->SetCanEverAffectNavigation(false);

	projectileFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Projectile FX Component"));
	projectileFX->SetupAttachment(RootComponent);

	projectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement Component"));
	projectileMovementComponent->UpdatedComponent = RootComponent;
	projectileMovementComponent->InitialSpeed = projectileSettings.ProjectileInitSpeed;
	projectileMovementComponent->MaxSpeed = projectileSettings.ProjectileInitSpeed;
	projectileMovementComponent->bRotationFollowsVelocity = true;
	projectileMovementComponent->bShouldBounce = true;

}

// Called when the game starts or when spawned
void AProjectileDefault::BeginPlay()
{
	Super::BeginPlay();

	projectileSphereCollision->OnComponentHit.AddDynamic(this, &AProjectileDefault::ProjectileCollisionSphereHit);
	projectileSphereCollision->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefault::ProjectileCollisionSphereBeginOverlap);
	projectileSphereCollision->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::ProjectileCollisionSphereEndOverlap);

	if (NameProjectile.ToString() != "") {
		UTopDownShooterGameInstance* gameInstance = Cast<UTopDownShooterGameInstance>(GetGameInstance());
		if (gameInstance) {
			gameInstance->GetProjectileInfoByName(NameProjectile, projectileSettings);
		}
	}

	projectileMovementComponent->InitialSpeed = projectileSettings.ProjectileInitSpeed;
	projectileMovementComponent->MaxSpeed = projectileSettings.ProjectileInitSpeed;
	
	SetLifeSpan(projectileSettings.ProjectileLifeTime);
}

// Called every frame
void AProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectileDefault::ProjectileCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid()) {
		EPhysicalSurface myPhysicalSurface = UGameplayStatics::GetSurfaceType(Hit);

		if (projectileSettings.HitDecals.Contains(myPhysicalSurface)) {
			UMaterialInterface* decalMaterial = projectileSettings.HitDecals[myPhysicalSurface];

			if (decalMaterial && OtherComp) {
				UGameplayStatics::SpawnDecalAttached(decalMaterial,
					FVector(20.0f),
					OtherComp,
					NAME_None,
					Hit.ImpactPoint,
					Hit.ImpactNormal.Rotation(),
					EAttachLocation::KeepWorldPosition,
					10.0f);
			}
		}
		if (projectileSettings.HitFXs.Contains(myPhysicalSurface)) {
			UParticleSystem* hitFX = projectileSettings.HitFXs[myPhysicalSurface];

			if (hitFX) {
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),
					hitFX,
					FTransform(Hit.ImpactNormal.Rotation(),
						Hit.ImpactPoint,
						FVector(1.0f)
					)
				);
			}
		}
		if (projectileSettings.ProjectileHitSound) {
			UGameplayStatics::PlaySoundAtLocation(
				GetWorld(),
				projectileSettings.ProjectileHitSound,
				Hit.ImpactPoint
			);
		}
	}
	UGameplayStatics::ApplyDamage(OtherActor, projectileSettings.ProjectileDamage, GetInstigatorController(), this, NULL);
	ImpactProjectile();
}

void AProjectileDefault::ProjectileCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void AProjectileDefault::ProjectileCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AProjectileDefault::InitProjectile(FProjectileInfo InitParam)
{
	projectileMovementComponent->InitialSpeed = InitParam.ProjectileInitSpeed;
	projectileMovementComponent->MaxSpeed = InitParam.ProjectileInitSpeed;
	//projectileMovementComponent->Velocity *= InitParam.ProjectileInitSpeed;
	SetLifeSpan(InitParam.ProjectileLifeTime);

	if (InitParam.ProjectileStaticMesh) {
		projectileMeshComponent->SetStaticMesh(InitParam.ProjectileStaticMesh);
		projectileMeshComponent->SetRelativeTransform(InitParam.ProjectileStaticMeshOffset);
	}
	else {
		projectileMeshComponent->DestroyComponent();
	}
	if (InitParam.ProjectileTrailFX) {
		projectileFX->SetTemplate(InitParam.ProjectileTrailFX);
		projectileFX->SetRelativeTransform(InitParam.ProjectileTrailFXOffset);
	}
	else {
		projectileFX->DestroyComponent();
	}

	projectileSettings = InitParam;
}

void AProjectileDefault::ImpactProjectile()
{
	this->Destroy();
}