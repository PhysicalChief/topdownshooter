// Fill out your copyright notice in the Description page of Project Settings.


#include "GrenadeProjectile.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

int32 DebugExplodeShow = 1;
FAutoConsoleVariableRef CVARDebugExplodeShow(
	TEXT("Debug.ExplodeShow"),
	DebugExplodeShow,
	TEXT("Show debug for explode"));

void AGrenadeProjectile::BeginPlay()
{
	Super::BeginPlay();
}

void AGrenadeProjectile::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	TimerExplode(deltaTime);

}

void AGrenadeProjectile::TimerExplode(float deltaTime)
{
	if (bTimerEnabled) {
		if (TimerToExplose >= TimeToExplose) {
			Explode();
		}
		else {
			TimerToExplose += deltaTime;
		}
	}
}

void AGrenadeProjectile::ProjectileCollisionSphereHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::ProjectileCollisionSphereHit(HitComponent, OtherActor, OtherComp, NormalImpulse, Hit);
	CurrentBounceCount++;
	if (CurrentBounceCount >= MaxBounceCount) {
		projectileMovementComponent->bShouldBounce = false;
	}
}

void AGrenadeProjectile::ImpactProjectile()
{
	bTimerEnabled = true;
}

void AGrenadeProjectile::Explode()
{
	if (DebugExplodeShow) {
		DrawDebugSphere(GetWorld(), GetActorLocation(), projectileSettings.ExplodeMinRadius, 12, FColor::Green, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), (projectileSettings.ExplodeMinRadius + projectileSettings.ExplodeMaxRadius) / 2, 12, FColor::Yellow, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), projectileSettings.ExplodeMaxRadius, 12, FColor::Red, false, 12.0f);
	}

	bTimerEnabled = false;

	if (projectileSettings.ExplodeFX) {
		UGameplayStatics::SpawnEmitterAtLocation(
			GetWorld(),
			projectileSettings.ExplodeFX,
			GetActorLocation(),
			GetActorRotation(),
			FVector(1.0f)
		);
	}
	if (projectileSettings.ExplodeSound) {
		UGameplayStatics::PlaySoundAtLocation(
			GetWorld(),
			projectileSettings.ExplodeSound,
			GetActorLocation()
		);
	}
	TArray<AActor*> actors;
	UGameplayStatics::ApplyRadialDamageWithFalloff(
		GetWorld(),
		projectileSettings.ExplodeMaxDamage,
		projectileSettings.ExplodeMaxDamage * 0.2f,
		GetActorLocation(),
		projectileSettings.ExplodeMinRadius,
		projectileSettings.ExplodeMaxRadius,
		5,
		NULL,
		actors,
		nullptr,
		nullptr
	);
	this->Destroy();
}

