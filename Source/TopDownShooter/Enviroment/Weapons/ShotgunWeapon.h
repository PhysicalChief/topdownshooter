// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WeaponDefault.h"
#include "ShotgunWeapon.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API AShotgunWeapon : public AWeaponDefault
{
	GENERATED_BODY()
	
};
