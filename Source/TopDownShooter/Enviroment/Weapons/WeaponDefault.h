// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "../../FunctionLibrary/Types.h"

#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSucces, int32, AmmoTake);

UCLASS()
class TOPDOWNSHOOTER_API AWeaponDefault : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWeaponDefault();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* BulletEjectionPoint = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	FWeaponInfo WeaponSetting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	FAddicionalWeaponInfo WeaponInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool bWeaponFiring = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool bWeaponIsReloading = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	float FireTimer = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	float ReloadTimer = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	FVector ShootEndLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool byBarrel = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float SizeVectorToChangeShootDirectionalLogic = 100.0f;

	//flags
	bool bBlockFire = false;
	//Dispersion
	bool bShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	bool bAimEnable = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	UAnimInstance* AnimInstance = nullptr;

	UPROPERTY(BlueprintAssignable)
	FOnWeaponReloadStart OnWeaponReloadStartDelegate;
	UPROPERTY(BlueprintAssignable)
	FOnWeaponReloadEnd OnWeaponReloadEndDelegate;
	UPROPERTY(BlueprintAssignable)
	FOnWeaponFireStart OnWeaponFireStartDelegate;

	bool bShellDropFlag = false;
	bool bBulletDropFlag = false;
	float ShellDropTimer = 0.0f;
	float BulletDropTimer = 0.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FName CurrentWeaponName;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void WeaponInit();

	void FireTick(float DeltaTime);

	void ReloadTick(float DeltaTime);

	void DispersionTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
	void SetWeaponStateFire(bool bIsFire);

	bool CheckWeaponCanFire();

	FProjectileInfo GetProjectileInfo();

	void Fire();

	void UpdateStateWeapon(EMovementState NewMovementState);

	void ChangeDispersion();

	UFUNCTION(BlueprintCallable)
	void SetWeaponSetting(FWeaponInfo NewWeaponinfo);

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound();

	UFUNCTION(BlueprintCallable)
	void ChangeWeaponRound(int32 count);

	UFUNCTION(BlueprintCallable)
	void InitWeaponReload();

	UFUNCTION(BlueprintCallable)
	void FinishReload();

	UFUNCTION(BlueprintCallable)
	FVector GetFireEndLocation() const;

	UFUNCTION(BlueprintCallable)
	FVector ApplyDispersionToShoot(FVector DirectionoShoot) const;

	UFUNCTION(BlueprintCallable)
	float GetCurrentDispersion() const;

	UFUNCTION(BlueprintCallable)
	void ChangeDispersionByShot();

	int8 GetCountProjectileByShoot() const;

	UFUNCTION(BlueprintCallable)
	void ShellDtopTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
	void BulletDropTick(float DeltaTime);

	void InitDropMesh(UStaticMesh* Mesh, FTransform Offset, FVector DropImpulse, float LifeTime, float RandomInpulseDirection, float PowerImpulse, float CustomMuss);

	void CancelReload();

	bool CanWeaponReload();

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	int32 GetAvailableAmmoForReload();
};