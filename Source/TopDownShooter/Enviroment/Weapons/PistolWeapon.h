// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WeaponDefault.h"
#include "PistolWeapon.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API APistolWeapon : public AWeaponDefault
{
	GENERATED_BODY()
	
};
