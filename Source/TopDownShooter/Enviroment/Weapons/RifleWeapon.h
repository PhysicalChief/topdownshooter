// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WeaponDefault.h"
#include "RifleWeapon.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API ARifleWeapon : public AWeaponDefault
{
	GENERATED_BODY()
	
};
