// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/Classes/Components/SphereComponent.h"
#include "PickUpActor.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API APickUpActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickUpActor();

	UPROPERTY(EditAnywhere, Category = "PickUpActor")
	USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickUpActor")
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditAnywhere, Category = "PickUpActor")
	USphereComponent* CollisionSphere;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "PickUpActor")
	virtual void PickUpSuccees();

};
