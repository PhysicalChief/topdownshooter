// Fill out your copyright notice in the Description page of Project Settings.


#include "OneSideOpenedDoor.h"
#include <Engine/TriggerBox.h>

#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/AudioComponent.h"
#include "GameFramework/Character.h"

AOneSideOpenedDoor::AOneSideOpenedDoor()
{
	PrimaryActorTick.bCanEverTick = true;

	doorMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorMeshComponent"));
	doorMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	inTriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("InTriggerBox"));
	inTriggerBox->AttachTo(doorMeshComponent);
	outTriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("OutTriggerBox"));
	outTriggerBox->AttachTo(doorMeshComponent);

	openDoorAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("openDoorAudioComponent"));
	openDoorAudioComponent->AttachTo(GetRootComponent());
	openDoorAudioComponent->bAutoActivate = false;
	closeDoorAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("closeDoorAudioComponent"));
	closeDoorAudioComponent->AttachTo(GetRootComponent());
	closeDoorAudioComponent->bAutoActivate = false;
}

void AOneSideOpenedDoor::BeginPlay()
{
	Super::BeginPlay();

	rotationStep = FRotator((maxRotator.Pitch) / (timeOpenClose * 100),
							(maxRotator.Yaw) / (timeOpenClose * 100),
							(maxRotator.Roll) / (timeOpenClose * 100));

	currentRotator = GetActorRotation();
	defaltRotation = GetActorRotation();
	FTransform newTranform = inTriggerBox->GetComponentTransform();
	inTrigger = GetWorld()->SpawnActor<ATriggerBox>(triggerClass, newTranform);
	inTrigger->SetActorEnableCollision(true);
	newTranform = outTriggerBox->GetComponentTransform();
	outTrigger = GetWorld()->SpawnActor<ATriggerBox>(triggerClass, newTranform);
	outTrigger->SetActorEnableCollision(true);

	if (inTrigger) {
		inTrigger->OnActorBeginOverlap.AddDynamic(this, &AOneSideOpenedDoor::OnOverlapBeginInTrigger);
		inTrigger->OnActorEndOverlap.AddDynamic(this, &AOneSideOpenedDoor::OnOverlapEndInTrigger);
	}
	if (outTrigger) {
		outTrigger->OnActorBeginOverlap.AddDynamic(this, &AOneSideOpenedDoor::OnOverlapBeginOutTrigger);
		outTrigger->OnActorEndOverlap.AddDynamic(this, &AOneSideOpenedDoor::OnOverlapEndOutTrigger);
	}
}

void AOneSideOpenedDoor::Open()
{
	currentRotator += rotationStep;
	doorMeshComponent->SetWorldRotation(currentRotator, false);

	if (maxRotator.Yaw >= 0) {
		if (currentRotator.Yaw >= defaltRotation.Yaw + maxRotator.Yaw) {
			doorState = EDoorStateStandart::DoorOpen;
			GetWorld()->GetTimerManager().ClearTimer(timerOpenClose);
		}
	}
	else {
		if (currentRotator.Yaw <= defaltRotation.Yaw + maxRotator.Yaw) {
			doorState = EDoorStateStandart::DoorOpen;
			GetWorld()->GetTimerManager().ClearTimer(timerOpenClose);
		}
	}
}

void AOneSideOpenedDoor::Close()
{
	currentRotator -= rotationStep;
	doorMeshComponent->SetWorldRotation(currentRotator, false);

	if (maxRotator.Yaw >= 0) {
		if (currentRotator.Yaw <= defaltRotation.Yaw) {
			closeDoorAudioComponent->Activate(false);
			doorState = EDoorStateStandart::DoorClose;
			GetWorld()->GetTimerManager().ClearTimer(timerOpenClose);
		}
	}
	else {
		if (currentRotator.Yaw >= defaltRotation.Yaw) {
			doorState = EDoorStateStandart::DoorClose;
			closeDoorAudioComponent->Activate(false);
			GetWorld()->GetTimerManager().ClearTimer(timerOpenClose);
		}
	}
}

void AOneSideOpenedDoor::OnOverlapBeginInTrigger(AActor* OverlappedActor, AActor* OtherActor)
{
	ACharacter* character = Cast<ACharacter>(OtherActor);
	if (character) {
		this->bDoorIn = true;
		chechDoorState();
	}
}

void AOneSideOpenedDoor::OnOverlapEndInTrigger(AActor* OverlappedActor, AActor* OtherActor)
{
	ACharacter* character = Cast<ACharacter>(OtherActor);
	if (character) {
		this->bDoorIn = false;
		chechDoorState();
	}
}

void AOneSideOpenedDoor::OnOverlapBeginOutTrigger(AActor* OverlappedActor, AActor* OtherActor)
{
	ACharacter* character = Cast<ACharacter>(OtherActor);
	if (character) {
		this->bDoorOut = true;
		chechDoorState();
	}
}

void AOneSideOpenedDoor::OnOverlapEndOutTrigger(AActor* OverlappedActor, AActor* OtherActor)
{
	ACharacter* character = Cast<ACharacter>(OtherActor);
	if (character) {
		this->bDoorOut = false;
		chechDoorState();
	}
}

void AOneSideOpenedDoor::chechDoorState()
{
	if (bDoorIn && !bDoorOut && (doorState != EDoorStateStandart::DoorOpen && doorState != EDoorStateStandart::DoorOpenAction)) {
		if (doorState == EDoorStateStandart::DoorCloseAction) 
			GetWorld()->GetTimerManager().ClearTimer(timerOpenClose);
		doorState = EDoorStateStandart::DoorOpenAction;
		GetWorld()->GetTimerManager().SetTimer(timerOpenClose, this, &AOneSideOpenedDoor::Open, 0.01f, true);
		openDoorAudioComponent->Activate(false);
	}
	if (!bDoorIn && bDoorOut && (doorState != EDoorStateStandart::DoorClose && doorState != EDoorStateStandart::DoorCloseAction)) {
		if (doorState == EDoorStateStandart::DoorOpenAction) 
			GetWorld()->GetTimerManager().ClearTimer(timerOpenClose);
		doorState = EDoorStateStandart::DoorCloseAction;
		GetWorld()->GetTimerManager().SetTimer(timerOpenClose, this, &AOneSideOpenedDoor::Close, 0.01f, true);
	}
}
