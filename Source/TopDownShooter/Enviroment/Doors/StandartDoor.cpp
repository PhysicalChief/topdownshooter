// Fill out your copyright notice in the Description page of Project Settings.


#include "StandartDoor.h"

// Sets default values
AStandartDoor::AStandartDoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AStandartDoor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AStandartDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AStandartDoor::Open()
{
}

void AStandartDoor::Close()
{
}

void AStandartDoor::OnOverlapBeginInTrigger(AActor* OverlappedActor, AActor* OtherActor)
{
}

void AStandartDoor::OnOverlapEndInTrigger(AActor* OverlappedActor, AActor* OtherActor)
{
}

void AStandartDoor::OnOverlapBeginOutTrigger(AActor* OverlappedActor, AActor* OtherActor)
{
}

void AStandartDoor::OnOverlapEndOutTrigger(AActor* OverlappedActor, AActor* OtherActor)
{
}

void AStandartDoor::chechDoorState()
{
}

