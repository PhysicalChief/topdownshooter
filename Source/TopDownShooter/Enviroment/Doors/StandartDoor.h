// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../../FunctionLibrary/Types.h"
#include "StandartDoor.generated.h"

class ATriggerBox;
class UBoxComponent;
class UStaticMeshComponent;

UCLASS()
class TOPDOWNSHOOTER_API AStandartDoor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AStandartDoor();

	UPROPERTY(EditDefaultsOnly)
		UBoxComponent* inTriggerBox;

	UPROPERTY(EditDefaultsOnly)
		UBoxComponent* outTriggerBox;

	UPROPERTY(BlueprintReadWrite)
		ATriggerBox* inTrigger;

	UPROPERTY(BlueprintReadWrite)
		ATriggerBox* outTrigger;

	UPROPERTY(EditDefaultsOnly)
		UStaticMeshComponent* doorMeshComponent;

	UPROPERTY(BlueprintReadWrite)
	bool bDoorIn = false;

	UPROPERTY(BlueprintReadWrite)
	bool bDoorOut = false;


	UPROPERTY(BlueprintReadWrite)
	EDoorStateStandart doorState = EDoorStateStandart::DoorClose;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Open();

	virtual void Close();

	virtual void OnOverlapBeginInTrigger(class AActor* OverlappedActor, class AActor* OtherActor);	

	virtual void OnOverlapEndInTrigger(class AActor* OverlappedActor, class AActor* OtherActor);

	virtual void OnOverlapBeginOutTrigger(class AActor* OverlappedActor, class AActor* OtherActor);

	virtual void OnOverlapEndOutTrigger(class AActor* OverlappedActor, class AActor* OtherActor);

	virtual void chechDoorState();
};
