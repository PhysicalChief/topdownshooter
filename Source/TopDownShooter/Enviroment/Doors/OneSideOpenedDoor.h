// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StandartDoor.h"
#include "OneSideOpenedDoor.generated.h"

/**
 *
 */
UCLASS()
class TOPDOWNSHOOTER_API AOneSideOpenedDoor : public AStandartDoor
{
	GENERATED_BODY()

public:
	AOneSideOpenedDoor();

	UPROPERTY()
	FTimerHandle timerOpenClose;

	UPROPERTY(EditAnywhere)
	FRotator currentRotator;

	UPROPERTY(EditAnywhere)
	FRotator maxRotator;

	UPROPERTY(EditAnywhere)
	FRotator defaltRotation;

	UPROPERTY(EditAnywhere)
	FRotator rotationStep;

	UPROPERTY(EditAnywhere)
	float timeOpenClose;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ATriggerBox> triggerClass;

	UPROPERTY(EditDefaultsOnly)
	class UAudioComponent* openDoorAudioComponent;

	UPROPERTY(EditDefaultsOnly)
	class UAudioComponent* closeDoorAudioComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	virtual void Open() override;

	virtual void Close() override;

	UFUNCTION()
	virtual void OnOverlapBeginInTrigger(AActor* OverlappedActor, AActor* OtherActor) override;
	UFUNCTION()
	virtual void OnOverlapEndInTrigger(AActor* OverlappedActor, AActor* OtherActor) override;

	UFUNCTION()
	virtual void OnOverlapBeginOutTrigger(AActor* OverlappedActor, AActor* OtherActor) override;
	UFUNCTION()
	virtual void OnOverlapEndOutTrigger(AActor* OverlappedActor, AActor* OtherActor) override;

	UFUNCTION(BlueprintCallable)
	virtual void chechDoorState() override;
};
