// Fill out your copyright notice in the Description page of Project Settings.


#include "TopDownShooterGameInstance.h"

bool UTopDownShooterGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	FWeaponInfo* NewWeaponInfo;

	if (WeaponInfoTable) {
		NewWeaponInfo = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
		if (NewWeaponInfo) {
			bIsFind = true;
			GetProjectileInfoByName(NewWeaponInfo->ProjectileDataTableName, NewWeaponInfo->ProjectileSetting);
			OutInfo = *NewWeaponInfo;
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::GetWeaponInfoByName - NameWeapon - Doesn't find"));
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
	}
	return bIsFind;
}

bool UTopDownShooterGameInstance::GetProjectileInfoByName(FName NameProjectile, FProjectileInfo& OutInfo)
{
	bool bIsFind = false;
	FProjectileInfo* NewProjectileInfo;

	if (WeaponInfoTable) {
		NewProjectileInfo = ProjectileInfoTable->FindRow<FProjectileInfo>(NameProjectile, "", false);
		if (NewProjectileInfo) {
			bIsFind = true;
			OutInfo = *NewProjectileInfo;
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::NewProjectileInfo - NameProjectile - Doesn't find"));
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::NewProjectileInfo - NameProjectile -NULL"));
	}
	return bIsFind;
}

bool UTopDownShooterGameInstance::GetDropItemInfoByName(FName NameItem, FDropItemStruct& OutInfo)
{
	bool bIsFind = false;
	FDropItemStruct* NewDropItem = nullptr;

	if (WeaponInfoTable) {
		FDropItemStruct* DropItemRow;
		if (DropItemTable) {
			TArray<FName> DropNames = DropItemTable->GetRowNames();
			for (int i = 0; i < DropNames.Num(); i++) {
				DropItemRow = DropItemTable->FindRow<FDropItemStruct>(DropNames[i], "");
				if (DropItemRow) {
					if (DropItemRow->WeaponSlot.NameItem == NameItem) {
						bIsFind = true;
						NewDropItem = DropItemRow;
						break;
					}
				}
			}
		}
		
		if (bIsFind) {
			OutInfo = *NewDropItem;
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::NewDropItem - NameProjectile - Doesn't find"));
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::NewDropItem - NameProjectile -NULL"));
	}
	return bIsFind;
}

