// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "../FunctionLibrary/Types.h"
#include "../Enviroment/Weapons/WeaponDefault.h"
#include "../Enviroment/Weapons/Projectile/ProjectileDefault.h"
#include "TopDownShooterGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTopDownShooterGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	//table
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = " WeaponSetting ")
	UDataTable* WeaponInfoTable = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = " ProjectileSetting ")
	UDataTable* ProjectileInfoTable = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = " DtopItemsSettings ")
	UDataTable* DropItemTable = nullptr;

public:
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);

	UFUNCTION(BlueprintCallable)
	bool GetProjectileInfoByName(FName NameWeapon, FProjectileInfo& OutInfo);

	UFUNCTION(BlueprintCallable)
	bool GetDropItemInfoByName(FName NameItem, FDropItemStruct& OutInfo);
};
